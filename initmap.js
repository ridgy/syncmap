var map;

function initmap() {

  // set up base layer(s) and marker layer(s)

  // create the tile layers with correct attribution

  var osmAttrib = 'Map data © <a href="http://osm.org/copyright">OpenStreetMap</a> contributors';
  var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  var osm = new L.TileLayer(osmUrl, {minZoom: 2, maxZoom: 18, attribution: osmAttrib});		

  var osmdeAttrib = osmAttrib + '; Map Tiles &copy; <a href="http://www.openstreetmap.de">OpenStreetMap Deutschland</a>, CC-BY-SA';
  var osmdeUrl = 'http://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png';
  var osmde = new L.TileLayer(osmdeUrl, {minZoom: 2, maxZoom: 18, attribution: osmdeAttrib});

  var esriAttrib = 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community';
  var esriUrl = 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}';
  var esri = new L.TileLayer(esriUrl, {minZoom: 2, maxZoom: 18, attribution: esriAttrib});

  // set up the map with initial zoom, center and visible layers

  //map = new L.Map('map', {center: [53.082, 8.8057], zoom: 18, layers: [osmde]});
  map = new L.Map('map', {center: [30, 0], zoom: 2, layers: [osmde]});
  // copy.innerHTML = osmdeAttrib;

  // set up the layers control for all layers

  var baseLayers = {"Deutscher Stil": osmde, "Original Mapnik": osm, "Satellit": esri};
  var overlays   = {};
  var layerswitcher = new L.control.layers(baseLayers, overlays);
  layerswitcher.addTo(map);
  map.on('baselayerchange', function(e) {
      var attrib = e.layer.options.attribution;
      //copy.innerHTML = attrib;
  }) 

  // additional controls (scale etc.)

  L.control.scale({metric: true, imperial: false, updateWhenIdle: true}).addTo(map);
}


