# SyncMap

Small JavaScript solution to synchronize video with GPX Track

## Intention

Some years ago a friend asked for a possibility to show a video (recorded when travelling) alongside the corresponding GPX track, to ease the editing of [OpenStreetMap](https://www.openstreetmap.org/) data.
I could not find a satisfying and open solution ready to go, so this small script was created, using the features of the <video> tag in HTML5 and some [leaflet](https://leafletjs.com/) libraries (`leaflet.js`, `togeojson.js`). 

## How to use

Having the files `index.html`, `SyncVideo.css`, `initmap.js`, `localFileGeoJSON.js` and `localFileVideoPlayer.js` as well as the directory `leaflet` somewhere on your server you could start with opening `index.html` in any complient actual browser (tested with Firefox and Chrome).

In the right panel, selecting `Select a track file` you can load your GPX file to the map, which will zoom and center to fit the track.
In the left panel, selecting `Select a video file` you can open your video, which should start immediately. As not all browsers do support all video formats, just try out. I could run successfully with *mp4* videos, but *webm* should work as well.  

The track marker runs synchronously to the video, supposing that the start time of the track is the same as the start time of the video. As this usually is not true, you can synchronize in modifying `Sync` to some value other than 0 (these are seconds in the start time difference).

Now when moving the video, the track marker will move to the corresponding track point. Vice versa, you can drag the track marker to any point of the track thus positioning the video to the corresponding frame.

You can try out [here](https://ridgy.gitlab.io/syncmap/).

## Standalone version

If you prefer an offline version, you can run a standalone version using [node.js](https://nodejs.org/)/[electron](https://www.electronjs.org/docs/tutorial/first-app). Some of the files needed are supplied here as well (`main.js`, `package.json` and `preload.js`). 
Just look at the corresponding documentation on how to run the application and [electron-packager](https://www.electronjs.org/docs/tutorial/application-distribution) to package it for different target architectures.

## Bugs

When the video is displayed using only software rendering (not using GPU acceleration), and the CPU is busy up to 100%, the display of the map may be flickering and jumping. Try to optimize the video for saving CPU load.

There must be no break, neither in the video nor the track; otherwise synchronisation could be lost.

No other bugs known at the moment, but surely lots of. The page layout and style may be heavily improved.

## Licenses

The libraries and data used undergo different licenses. See the respective information inside the libraries. Map data: https://www.openstreetmap.org/copyright. 