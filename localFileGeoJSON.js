(function localFileGeoJSON(){
  'use strict';

  // global layers, so we can remove them at reload of track
  var geojsonLayer;
  var posmarker;

  // - make geoJSON object out of track, show on map
  // - create position array
  // - define the handlers for video events
  var parseTrackString = function (fcontent, ext) {
      if (typeof fcontent == 'string') {
        fcontent = ( new window.DOMParser() ).parseFromString(fcontent, "text/xml");  // return xml object
      }
      var geojson = toGeoJSON[ext](fcontent);                                         // return geoJSON object
      var myMap = window.map;

      // Remove previos (old) layer and marker
      if (geojsonLayer) { myMap.removeLayer(geojsonLayer); }
      if (posmarker) { myMap.removeLayer(posmarker); }

      // Show track in map
      geojsonLayer = L.geoJson(geojson).addTo(myMap);
      myMap.fitBounds(geojsonLayer.getBounds());

      // Build array of time/lon/lat tripels for all the trackpoints. This
      // depends on toGeoJSON saving time values as additional property arrays.
      // The absolute times are converted to seconds relative to the start,
      // so this should correlate with the video times.
      // Up to now we only deal with one (the first) feature.
      //
      // We have to distinguish between "LineString" and "MultiLineString"

      var mls = (geojson.features[0].geometry.type == "MultiLineString");
      var i, j, point, pointlist = [], mintime;
      if (mls) {
        mintime = Date.parse(geojson.features[0].properties.coordTimes[0][0])/1000;
      } else {
        mintime = Date.parse(geojson.features[0].properties.coordTimes[0])/1000;
      }
      var coordlen = geojson.features[0].geometry.coordinates.length;
      for (i = 0; i < coordlen; i++) {
        if (mls) {
          var linelen = geojson.features[0].geometry.coordinates[i].length;
          for (j = 0; j < linelen; j++) {
            point = [Date.parse(geojson.features[0].properties.coordTimes[i][j])/1000 - mintime,
                                  L.latLng(geojson.features[0].geometry.coordinates[i][j][1],
                                           geojson.features[0].geometry.coordinates[i][j][0])];
            pointlist.push(point);
          }
        } else {
          point = [Date.parse(geojson.features[0].properties.coordTimes[i])/1000 - mintime,
                                L.latLng(geojson.features[0].geometry.coordinates[i][1],
                                         geojson.features[0].geometry.coordinates[i][0])];
          pointlist.push(point);
        }
      }

      // global variables used by listeners
      var plmax = pointlist.length-1;
      var currentdelta = 0;
      var currentpos = 0;
      var vtime1 = 0;
      var seeked = false;
      var paused = false;
      var synced = false;
      var myVideo = document.getElementById("video");

      // Create position marker at point[0]
      posmarker = L.marker(pointlist[0][1], {draggable:'true'}).addTo(myMap);
      // listener for marker drags
      posmarker.addEventListener('dragstart', function(e) {
        myVideo.pause();
        paused = true;
      });
      posmarker.addEventListener('dragend', function(e) {
        var marker = e.target;
        var pos = marker.getLatLng();
        var dist, distmin = 100000;
        for (i = 0; i < pointlist.length; i++) {
          dist = pos.distanceTo(pointlist[i][1]);
          if (dist < distmin) {
            distmin = dist;
            j = i;
          }
        }
        marker.setLatLng(pointlist[j][1]);
        var vtime = pointlist[j][0] - currentdelta;
        if (vtime < 0) vtime = 0;
        if (vtime > myVideo.duration) vtime = myVideo.duration;
        // Not all browsers support fastsync()
        if (typeof myVideo.fastSeek === 'function') {
          myVideo.fastSeek(vtime);
        }
        else {
          myVideo.currentTime = vtime;
        }
      });

      // Main function used by listeners to update marker position
      var updatePosition = function(vtime) {
        var gtime = pointlist[currentpos][0] - currentdelta;
        if (seeked || paused || synced) {
          while (vtime < gtime && currentpos > 0) {
            currentpos--;
            gtime = pointlist[currentpos][0] - currentdelta;
          }
          seeked = false;
          synced = false;
        }
        while (vtime > gtime && currentpos < plmax) {
          currentpos++;
          gtime = pointlist[currentpos][0] - currentdelta;
        }
        var lat = pointlist[currentpos][1].lat, lon = pointlist[currentpos][1].lng;
        // Linear interpolation between two trackpoints, if it is not trackstart.
        // We assume different timestamps for different trackpoints.
        if (currentpos > 0 && gtime > vtime) {
           var factor = (gtime - vtime)/(gtime - pointlist[currentpos-1][0] + currentdelta);
           lat = lat - factor * (lat - pointlist[currentpos-1][1].lat);
           lon = lon - factor * (lon - pointlist[currentpos-1][1].lng);
        }
        myMap.panTo([lat, lon]);
        posmarker.setLatLng([lat, lon]);
        displayMessage("vmessage", vtime.toFixed(2) + "s", false);
        displayMessage("gmessage", (gtime+currentdelta).toFixed(2) + "s ("
                                + currentpos + "/" + plmax +"); lat: "
                                + lat.toFixed(6) + ", lon: " + lon.toFixed(6), false);
      }

      // Listeners for video events: play, pause and seeked
      myVideo.addEventListener('seeked', function(e) {
        seeked = true;
      });
      myVideo.addEventListener('pause', function(e) {
        paused = true;
      });
      myVideo.addEventListener('play', function(e) {
        paused = false;
      });
      // main listener for video timeupdate event
      myVideo.addEventListener('timeupdate', function(e) {
        var vtime = myVideo.currentTime;
        if (Math.abs(vtime - vtime1) > 0.1) {
          vtime1 = vtime;
          updatePosition(vtime);
        }
      });
      // listener for synchronisation updates.
      var mySyncer = document.getElementById("syncer");
      mySyncer.value = currentdelta;
      mySyncer.addEventListener('change', function(e) {
        var newdelta = Number(mySyncer.value);
        if (newdelta < currentdelta) { synced = true; }
        currentdelta = newdelta;
        // update marker, if video is paused
        if (paused) {
          updatePosition(myVideo.currentTime);
        }
      });
  }

  var getTrackFile = function (e) {
    var file = this.files[0];
    var ext = file.name.split('.').pop();
    if (ext !== "gpx" && ext !== "kml") {
      displayMessage("gmessage", "File type not supported: ." + file.type + '. (' + ext + ')', true);
      return;
    }
    displayMessage("gmessage", "File type supported: ." + file.type + '. (' + ext + ')', false);
    // Read selected file using HTML5 File API.
    // reader.result is not valid before reader.onloadend
    var reader = new FileReader();
    reader.onloadend = function (e) {
      var filecontent = reader.result;
      return parseTrackString(filecontent, ext);
    }
    reader.readAsText(file);
  }

  var inputNode = document.getElementById('ginput');
  inputNode.addEventListener('change', getTrackFile, false);
})()
