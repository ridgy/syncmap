(function localFileVideoPlayer() {
  'use strict';
  var URL = window.URL || window.webkitURL;
  var playSelectedFile = function (event) {
    var file = this.files[0];
    var type = file.type;
    var videoNode = document.getElementById('video');
    var canPlay = videoNode.canPlayType(type);
    if (canPlay === '') canPlay = 'no';
    var message = 'Can play type "' + type + '": ' + canPlay;
    var isError = canPlay === 'no';
    displayMessage("vmessage", message, isError);

    if (isError) {
      return;
    }

    var fileURL;
    if (typeof fileURL !== "undefined") {
      URL.releaseObjectURL(fileURL);
    }
    fileURL = URL.createObjectURL(file);
    videoNode.src = fileURL;
  }
  var inputNode = document.getElementById('vinput');
  inputNode.addEventListener('change', playSelectedFile, false);
})()
